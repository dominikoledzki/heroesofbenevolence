'use strict';

angular.module('HoB', [
  'ngResource',
  'ui.router'
])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        controller: 'HomeCtrl',
        templateUrl: 'views/home.html'
      })
      .state('quest', {
        url: '/quest/:id',
        controller: 'QuestCtrl',
        templateUrl: 'views/quest.html'
      })
      .state('new-quest', {
        url: '/new-quest',
        controller: 'NewQuestCtrl',
        templateUrl: 'views/newQuest.html'
      })
      .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        templateUrl: 'views/login.html'
      })
      .state('logout', {
        url: '/logout',
        controller: 'LogoutCtrl'
      })
      .state('payment', {
        url: '/payment',
        controller: 'PaymentCtrl',
        templateUrl: 'views/payment.html'
      })
      .state('quest-started', {
        url: '/quest-started',
        templateUrl: 'views/questStarted.html'
      })
      .state('quests', {
        url: '/quests',
        controller: 'QuestsCtrl',
        templateUrl: 'views/quests.html'
      })
      .state('payment-created', {
        url: '/quest-created',
        templateUrl: 'views/questCreated.html'
      });

      $urlRouterProvider.otherwise('/')
  })
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push(function() {
      return {
        'request': function(config) {
          config.withCredentials = true;
          return config;
        }
      };
    });
  })
  .run(function ($rootScope, User, Config) {
    $rootScope.user = User;
    $rootScope.config = Config;
  })
  .run(function (User) {
    User.fetchData().then(function () {
       if (User.getData().hasOwnProperty('id')){
         User.authenticate();
       }
    });
  });
