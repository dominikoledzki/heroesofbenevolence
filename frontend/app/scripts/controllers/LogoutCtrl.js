angular.module('HoB')
  .controller('LogoutCtrl', function ($scope, $http, User, Config, $state) {
    $http.get(Config.BACKEND_URL + 'logout/')
      .then(function () {
        User.logout();
        $state.go('home');
      });

  });
