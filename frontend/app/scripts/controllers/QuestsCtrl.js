angular.module('HoB')
  .controller('QuestsCtrl', function ($scope, CurrentQuests, $http, $stateParams, Charities, Config, $state) {
    $scope.quests = CurrentQuests.query();
    $scope.charities = Charities.query();

    $scope.confirmQuest = function (quest) {
      $http.post(Config.BACKEND_URL + 'closeQuest/', {
        id: quest.id
      }).then(function () {
          quest.status = 'closed';
          quest.confirmed = true;
        });
    };

    $scope.donate = function (quest, charity) {
      $http.post(Config.BACKEND_URL + 'payQuest/', {
        charityId: charity.id,
        questId: quest.id
      })
        .then(function () {
          quest.status = 'paid';
          quest.donated = true;
        });
    };
  })
  .service('CurrentQuests', function ($resource, Config) {
    return $resource(Config.BACKEND_URL + 'getCurrentQuests/')
  })
  .service('Charities', function ($resource, Config) {
    return $resource(Config.BACKEND_URL + 'getCharities/')
  });
