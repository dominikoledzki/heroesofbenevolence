angular.module('HoB')
  .controller('LoginCtrl', function ($scope, $http, Config, User, $state) {
    $scope.user = {};
    $scope.login = function () {
      $http.post(Config.BACKEND_URL + 'login/', $scope.user).then(function () {
        User.authenticate();
        User.fetchData().then(function () {
          $state.go('home');
        });
      }, function () {
        alert('failed');
      });
    };
  });
