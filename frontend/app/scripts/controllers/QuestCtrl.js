angular.module('HoB')
  .controller('QuestCtrl', function ($scope, Quest, $http, $stateParams, Config, $state) {
    $scope.quest = Quest.get({
      id: $stateParams.id
    });

    $scope.startQuest = function () {
      $http.post(Config.BACKEND_URL + 'assignQuest/', {
        questId: $scope.quest.id
      })
        .then(function () {
          $state.go('quest-started');
        });
    }

  })
  .service('Quest', function ($resource, Config) {
    return $resource(Config.BACKEND_URL + 'getQuest/:id')
  });
