'use strict';

angular.module('HoB')
  .controller('HomeCtrl', function ($scope, Quests, Heroes) {
    $scope.heroes = Heroes.query();

    $scope.quests = Quests.query();
  })
  .service('Quests', function ($resource, Config) {
    return $resource(Config.BACKEND_URL + 'getLatestQuests/');
  })
  .service('Heroes', function ($resource, Config) {
    return $resource(Config.BACKEND_URL + 'getTopHeroes/');
  });
