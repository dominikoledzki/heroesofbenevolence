angular.module('HoB')
  .controller('PaymentCtrl', function ($scope, $http, $state, Config) {

    var token = null;

    $http.get(Config.BACKEND_URL + 'getClientToken/').then(function (response) {
      token = response.data.clientToken;
      braintree.setup(token, "dropin", {
        container: "paymentDropin",
        paymentMethodNonceReceived: function (event, nonce) {
          $http.post(Config.BACKEND_URL + 'providePaymentData/', {
            paymentMethodNonce: nonce
          })
            .then(function () {
              $state.go('payment-created');
            });
        }
      });
    });
  });