angular.module('HoB')
  .controller('NewQuestCtrl', function ($scope, $http, $state, Config) {
    $scope.quest = {};

    $scope.getFeedbackClass = function (field) {
      return field.$visited && field.$invalid && 'has-error';
    };

    $scope.createQuest = function () {
      $http.post(Config.BACKEND_URL + 'addQuest/', $scope.quest).then(function () {
         $state.go('payment');
      });
    };
  });