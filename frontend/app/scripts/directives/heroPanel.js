angular.module('HoB')
  .directive('heroPanel', function () {
    return {
      scope: {
        hero: '='
      },
      templateUrl: '/views/heroPanel.html'
    }
  });