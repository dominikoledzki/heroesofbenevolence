angular.module('HoB')
  .directive('lordPanel', function () {
    return {
      scope: {
        lord: '='
      },
      templateUrl: '/views/lordPanel.html'
    }
  });