angular.module('HoB')
  .directive('input', function () {
    return {
      restrict: 'E',
      priority: -1000,
      require: '?ngModel',
      link: function (scope, element, attrs, ngModel) {

        if (!ngModel){
          return;
        }

        element.on('blur', function () {
           scope.$apply(function () {
             ngModel.$visited = true;
           });
        });
      }
    }
  })
  .directive('textarea', function () {
    return {
      restrict: 'E',
      priority: -1000,
      require: '?ngModel',
      link: function (scope, element, attrs, ngModel) {

        if (!ngModel){
          return;
        }

        element.on('blur', function () {
          scope.$apply(function () {
            ngModel.$visited = true;
          });
        });
      }
    }
  });