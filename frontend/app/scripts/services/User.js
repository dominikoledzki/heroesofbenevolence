angular.module('HoB')
  .service('User', function (Config, $http) {
    var authenticated = false;
    var data = null;

    this.isAuthenticated = function () {
      return authenticated;
    };

    this.authenticate = function () {
      authenticated = true;
    };

    this.logout = function () {
      authenticated = false;
    };

    this.fetchData = function () {
     return $http.get(Config.BACKEND_URL + 'getCurrentUserData/').then(function (response) {
        data = response.data;
     });
    };

    this.getData = function () {
      return data;
    };

    this.isLandlord = function () {
      return this.isAuthenticated() && this.getData().type == 'landlord';
    };

  });