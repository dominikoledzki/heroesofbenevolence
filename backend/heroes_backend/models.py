from django.db import models
from django.contrib.auth.models import User

class Charity(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)
    bt_merchant_id = models.CharField(max_length=32)
    total = models.IntegerField(default=0)


    def getJson(self):
        dict = {}
        dict["id"] = self.id
        dict["name"] = self.name
        dict["bt_merchant_id"] = self.bt_merchant_id
        dict["total"] = self.total
        return dict

class Landlord(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length=200)
    reputation = models.IntegerField(default=0)
    bt_customer_id = models.CharField(max_length=20)
    bt_method_token = models.CharField(max_length=20)



    def getJson(self):
        dict = {}
        dict["id"] = self.id
        dict["name"] = self.name
        dict["reputation"] = self.reputation
        dict["bt_customer_id"] = self.bt_customer_id
        dict["bt_method_token"] = self.bt_method_token
        return dict

class Hero(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length=200)
    reputation = models.IntegerField(default=0)


    def getJson(self):
        dict = {}
        dict["id"] = self.id
        dict["name"] = self.name
        dict["reputation"] = self.reputation
        return dict

class Quests(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)
    reward = models.IntegerField(default=0)
    place = models.CharField(max_length=1000)
    time = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    owner = models.ForeignKey(Landlord)
    performer = models.ForeignKey(Hero, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


    def getJson(self):
        dict = {}
        dict["id"] = self.id
        dict["name"] = self.name
        dict["description"] = self.description
        dict["reward"] = self.reward
        dict["place"] = self.place
        dict["time"] = self.time
        dict["status"] = self.status
        dict["owner"] = self.owner.getJson() if self.owner is not None else None
        dict["performer"] = self.performer.getJson() if self.performer is not None else None
        return dict


# Create your models here.
