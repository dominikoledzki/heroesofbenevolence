from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.forms import model_to_dict
from django.http import HttpResponse
from django.shortcuts import render
from models import *
import braintree
import json


def index(request):
    response_data = {}
    response_data["key"] = "value"
    response_data["second_key"] = "other_value"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def get_charities(request):
    response_data = Charity.objects.all()
    charDict = [c.getJson() for c in response_data]
    return HttpResponse(json.dumps(charDict), content_type="application/json")


def get_heroes(request):
    response_data = Hero.objects.all()
    heroDict = [h.getJson() for h in response_data]
    return HttpResponse(json.dumps(heroDict), content_type="application/json")


def get_landlords(request):
    response_data = Landlord.objects.all()
    llDict = [l.getJson() for l in response_data]
    return HttpResponse(json.dumps(llDict), content_type="application/json")


def get_quests(request):
    response_data = Quests.objects.all()
    qDict = [q.getJson() for q in response_data]
    return HttpResponse(json.dumps(qDict), content_type="application/json")


def add_quest(request):
    values = json.loads(request.body)
    quest = Quests()
    quest.time = values["time"]
    quest.reward = values["reward"]
    quest.description = values["description"]
    quest.name = values["name"]
    quest.place = values["place"]
    quest.status = "pending"
    quest.owner = Landlord.objects.get(pk=request.user.id)
    quest.performer = Hero.objects.get(pk=values["performerId"]) if "performerId" in values else None
    quest.save()
    response = {"status": "ok", "id": quest.id}
    return HttpResponse(json.dumps(response), "application/json")

def get_current_quests(request):
    user = request.user
    if user.is_authenticated():
        try:
            response = Quests.objects.filter(owner=user.landlord).order_by('-created_at')
        except ObjectDoesNotExist:
            response = Quests.objects.filter(performer=user.hero).order_by('-created_at')
    else:
        response = {}
    dict = [r.getJson() for r in response]
    return HttpResponse(json.dumps(dict), "application/json")

def assign_quest(request):
    values = json.loads(request.body)
    quest = Quests.objects.get(pk=values["questId"])
    quest.performer = request.user.hero
    quest.status = "assigned"
    quest.save()
    response = {"status": "ok"}
    return HttpResponse(json.dumps(response), "application/json")


def close_quest(request):
    values = json.loads(request.body)
    quest = Quests.objects.get(pk=values["id"])
    quest.status = "closed"
    quest.save()
    response = {"status" : "ok"}
    return HttpResponse(json.dumps(response), "application/json")


def pay_quest(request):
    values = json.loads(request.body)
    quest = Quests.objects.get(pk=values["questId"])
    charity = Charity.objects.get(pk=values["charityId"])

    reward = quest.reward
    merchant_id = charity.bt_merchant_id
    token = quest.owner.bt_method_token

    braintree.Transaction.sale({
        "payment_method_token": token,
        "amount": '%d.00' % reward,
        "merchant_account_id": merchant_id,
        "service_fee_amount": "0.00",
        "options": {
            "submit_for_settlement": True
        },
    })
    owner = quest.owner
    performer = quest.performer
    owner.reputation += reward
    performer.reputation += reward
    owner.save()
    performer.save()

    quest.status = "paid"
    quest.save()
    response = {"status" : "ok"}
    return HttpResponse(json.dumps(response), "application/json")


def cancel_quest(request):
    values = json.loads(request.body)
    quest = Quests.objects.get(pk=values["questId"])
    
    quest.delete()
    response = {"status": "ok"}
    return HttpResponse(json.dumps(response), "application/json")


def get_client_token(request):
    client_token = braintree.ClientToken.generate()
    response = {"clientToken": client_token}
    return HttpResponse(json.dumps(response), "application/json")


def create_test_data(request):
    user = User.objects.create_user('LordFarquad', 'lennon@thebeatles.com', 'zaq')
    user1 = User.objects.create_user('landlord2', 'lennon@thebeatles.com', 'zaq')
    user2 = User.objects.create_user('Shrek', 'lennon@thebeatles.com', 'zaq')
    user3 = User.objects.create_user('hero2', 'lennon@thebeatles.com', 'zaq')

    user.save()
    user1.save()
    user2.save()
    user3.save()

    landlord = Landlord()
    landlord.user = User.objects.get(username="LordFarquad")
    landlord.name = "LordFarquad"
    landlord.reputation = 0
    landlord.bt_customer_id = "66946941"
    landlord.save()

    landlord2 = Landlord()
    landlord2.user = User.objects.get(username="landlord2")
    landlord2.name = "landlord2"
    landlord2.reputation = 10
    landlord2.bt_customer_id = "38291489"
    landlord2.save()

    hero = Hero()
    hero.reputation = 0
    hero.name = "Shrek"
    hero.user = User.objects.get(username="Shrek")
    hero.save()

    hero2 = Hero()
    hero2.reputation = 10
    hero2.name = "hero2"
    hero2.user = User.objects.get(username="hero2")
    hero2.save()

    quest1 = Quests()
    quest1.name = "Mow the lawn"
    quest1.description = "Use my lawn mower and my petrol to mow 2ha of grass area. The area is filled with rocks" \
                         "so you will have to work really hard. The mower you are going to use is powered by petrol" \
                         "so make sure you are not allergic to the fumes. The mower is really powerful and make a lot" \
                         "of noise but I will provide you the ear buds and safety glasses. The prize if very high, so come" \
                         "join us. We have cookies."
    quest1.owner = landlord
    quest1.place = "Mokotowska 1"
    quest1.reward = 500
    quest1.status = "pending"
    quest1.time = "13.07.2014"
    quest1.save()

    quest3 = Quests()
    quest3.name = "Help during career fair"
    quest3.description = "We need a volunteer that will give leaflets at the career fair." \
                         "Apart from reward each volunteer will receive free " \
                         "lunch. During the fair you might be asked to say a few words to a camera. After the " \
                         "fair you will be able to join us for a free beer. We guarantee great fun and extreme experience" \
                         "in giving out leaflets"
    quest3.owner = landlord2
    quest3.place = "al. Jerozolimskie 124"
    quest3.reward = 500
    quest3.status = "pending"
    quest3.time = "13.07.2014"
    quest3.save()

    quest2 = Quests()
    quest2.name = "Walk two dogs"
    quest2.description = "Could you please take my dogs for a walk? They are two very friendly rottweillers. They have " \
                         "sharp teeth and are very strong. They bite a lot and might get you injured. Luckily, there" \
                         "were no case that the walker was killed be the dogs but who knows. If you will be aggresive then" \
                         "you will probably lose an arm - they are a bit like wookies. I do not cover the treatment" \
                         "costs. Quest is only for the bravest. Reward is minimal, you do it for eternal glory."
    quest2.owner = landlord
    quest2.performer = hero
    quest2.place = "Mokotowska 1"
    quest2.reward = 20
    quest2.status = "assigned"
    quest2.time = "13.07.2014"
    quest2.save()

    # quest4 = Quests()
    # quest4.name = "Walk two dogs"
    # quest4.description = "Could you please take my dogs for a walk? They are two very friendly rottweillers. They have " \
    #                      "sharp teeth and are very strong. They bite a lot and might get you injured. Luckily, there" \
    #                      "were no case that the walker was killed be the dogs but who knows. If you will be aggresive then" \
    #                      "you will probably lose an arm - they are a bit like wookies. I do not cover the treatment" \
    #                      "costs. Quest is only for the bravest. Reward is minimal, you do it for eternal glory."
    # quest4.owner = landlord
    # quest4.performer = hero
    # quest4.place = "Mokotowska 1"
    # quest4.reward = 20
    # quest4.status = "assigned"
    # quest4.time = "13.07.2014"
    # quest4.save()
    #
    # quest5 = Quests()
    # quest5.name = "Walk two dogs"
    # quest5.description = "Could you please take my dogs for a walk? They are two very friendly rottweillers. They have " \
    #                      "sharp teeth and are very strong. They bite a lot and might get you injured. Luckily, there" \
    #                      "were no case that the walker was killed be the dogs but who knows. If you will be aggresive then" \
    #                      "you will probably lose an arm - they are a bit like wookies. I do not cover the treatment" \
    #                      "costs. Quest is only for the bravest. Reward is minimal, you do it for eternal glory."
    # quest5.owner = landlord
    # quest5.performer = hero
    # quest5.place = "Mokotowska 1"
    # quest5.reward = 20
    # quest5.status = "assigned"
    # quest5.time = "13.07.2014"
    # quest5.save()
    #
    # quest6 = Quests()
    # quest6.name = "Walk two dogs"
    # quest6.description = "Could you please take my dogs for a walk? They are two very friendly rottweillers. They have " \
    #                      "sharp teeth and are very strong. They bite a lot and might get you injured. Luckily, there" \
    #                      "were no case that the walker was killed be the dogs but who knows. If you will be aggresive then" \
    #                      "you will probably lose an arm - they are a bit like wookies. I do not cover the treatment" \
    #                      "costs. Quest is only for the bravest. Reward is minimal, you do it for eternal glory."
    # quest6.owner = landlord
    # quest6.performer = hero
    # quest6.place = "Mokotowska 1"
    # quest6.reward = 20
    # quest6.status = "closed"
    # quest6.time = "13.07.2014"
    # quest6.save()
    #
    # quest7 = Quests()
    # quest7.name = "Walk two dogs"
    # quest7.description = "Could you please take my dogs for a walk? They are two very friendly rottweillers. They have " \
    #                      "sharp teeth and are very strong. They bite a lot and might get you injured. Luckily, there" \
    #                      "were no case that the walker was killed be the dogs but who knows. If you will be aggresive then" \
    #                      "you will probably lose an arm - they are a bit like wookies. I do not cover the treatment" \
    #                      "costs. Quest is only for the bravest. Reward is minimal, you do it for eternal glory."
    # quest7.owner = landlord
    # quest7.performer = hero
    # quest7.place = "Mokotowska 1"
    # quest7.reward = 20
    # quest7.status = "closed"
    # quest7.time = "13.07.2014"
    # quest7.save()

    charity = Charity()
    charity.name = "Dog shelter"
    charity.bt_merchant_id = "dogshelter"
    charity.save()

    charity2 = Charity()
    charity2.name = "Hospital for children"
    charity2.bt_merchant_id = "hospital"
    charity2.save()

    response = [hero.getJson(), hero2.getJson(), landlord.getJson(), landlord2.getJson()]

    return HttpResponse(json.dumps(response), "application/json")


def get_current_user_data(request):
    user = request.user
    if user.is_authenticated():
        try:
            response = user.landlord.getJson()
            response["type"] = "landlord"
        except ObjectDoesNotExist:
            response = user.hero.getJson()
            response["type"] = "hero"
    else:
        response = {}
    return HttpResponse(json.dumps(response), "application/json")

def get_quest(request, id):
    quest = Quests.objects.get(pk=id)
    return HttpResponse(json.dumps(quest.getJson()), "application/json")

def provide_payment_data(request):
    values = json.loads(request.body)
    user = request.user
    payment_method_nonce = values["paymentMethodNonce"]

    if user.is_authenticated():
        try:
            customer_id = user.landlord.bt_customer_id
            result = braintree.PaymentMethod.create({
                "customer_id": customer_id,
                "payment_method_nonce": payment_method_nonce
            })
            token = result.payment_method.token
            user.landlord.bt_method_token = token
            user.landlord.save()
            user.save()
            response = {"status": "ok"}

        except ObjectDoesNotExist:
            response = {}
    else:
        response = {}

    return HttpResponse(json.dumps(response), "application/json")


def get_user(request):
    return HttpResponse(request.user)


def logout_user(request):
    logout(request)
    return HttpResponse()


def login_user(request):
    requestBody = request.body
    credentials = json.loads(requestBody)

    user = authenticate(username=credentials["username"], password=credentials["password"])

    login(request, user)
    return HttpResponse()

def get_latest_quests(request):
    response = Quests.objects.all().order_by('-created_at')[:3]
    qDict = [q.getJson() for q in response]
    return HttpResponse(json.dumps(qDict), "application/json")

def get_top_heroes(request):
    response = Hero.objects.all().order_by('-reputation')
    hDict = [h.getJson() for h in response]
    return HttpResponse(json.dumps(hDict), "application/json")



