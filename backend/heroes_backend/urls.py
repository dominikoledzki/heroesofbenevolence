from django.conf.urls import patterns, url
from heroes_backend import views

urlpatterns = patterns('',
    url(r'^addQuest/', views.add_quest, name='add_quest'),
    url(r'^getUser/', views.get_user),
    url(r'^login/', views.login_user),
    url(r'^logout/', views.logout_user),
    url(r'^getTopHeroes/', views.get_top_heroes),
    url(r'^getLatestQuests/', views.get_latest_quests),
    url(r'^createTestData/', views.create_test_data, name='create_test_data'),
    url(r'^getCurrentUserData/', views.get_current_user_data, name='get_current_user'),
    url(r'^getQuests/', views.get_quests, name='get_quests'),
    url(r'^payQuest/', views.pay_quest, name='pay_quest'),
    url(r'^getQuest/(\d+)', views.get_quest, name='get_quest'),
    url(r'^cancelQuest', views.cancel_quest),
    url(r'^closeQuest', views.close_quest),
    url(r'^getLandlords/', views.get_landlords, name='get_landlords'),
    url(r'^getHeroes/', views.get_heroes, name='get_heroes'),
    url(r'^getCharities/', views.get_charities, name='get_charities'),
    url(r'^getClientToken/', views.get_client_token, name='get_client_token'),
    url(r'^providePaymentData/', views.provide_payment_data, name='provide_payment_data'),
    url(r'^getCurrentQuests/', views.get_current_quests),
    url(r'^assignQuest/', views.assign_quest, name='assign_quest'),
    url(r'^$', views.index, name='index')
)