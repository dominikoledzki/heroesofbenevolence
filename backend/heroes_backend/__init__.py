import braintree
from django.conf import settings

braintree.Configuration.configure(braintree.Environment.Sandbox,
                                  merchant_id=settings.BT_MERTCHANT_ID,
                                  public_key=settings.BT_PUBLIC_KEY,
                                  private_key=settings.BT_PRIVATE_KEY)