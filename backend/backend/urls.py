from django.conf.urls import patterns, include, url
#import heroes_backend.views
from django.contrib import admin
# import django.contrib.auth.views.login

#from backend.heroes_backend import views

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^accounts/login/', 'django.contrib.auth.views.login'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^api/', include("heroes_backend.urls")),
    url(r'^admin/', include(admin.site.urls)),
)
